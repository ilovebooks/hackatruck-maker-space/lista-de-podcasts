//
//  PodcastTableViewController.swift
//  PodcastTableVIew
//
//  Created by Student on 14/09/22.
//  Copyright © 2022 Student. All rights reserved.
//

import UIKit

class PodcastTableViewController: UITableViewController, UISearchBarDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    
    var podcasts = [Podcast]()
    var filteredPodcasts: [Podcast]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        podcasts = PodcastDAO.getList()
        filteredPodcasts = podcasts
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredPodcasts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "podcast", for: indexPath)
        
        if let podcastCell = cell as? PodcastTableViewCell {
            let podcast = filteredPodcasts[indexPath.row]
            podcastCell.podcastNameLabel.text = podcast.podcastName
            podcastCell.podcastEpisodesLabel.text = "\(podcast.podcastEpisodes) episódios"
            podcastCell.podcastLogoImageVIew.image = UIImage(named: podcast.podcastLogo)
            
            return podcastCell
            
        }

        return cell
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            podcasts.remove(at: indexPath.row)
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    // This method updates filteredPodcasts based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredPodcasts = searchText.isEmpty ? podcasts : podcasts.filter({
            (podcast: Podcast) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return podcast.podcastName.range(of: searchText, options: .caseInsensitive) != nil
        })

        tableView.reloadData()
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
