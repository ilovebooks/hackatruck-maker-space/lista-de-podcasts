//
//  Podcast.swift
//  PodcastTableVIew
//
//  Created by Student on 14/09/22.
//  Copyright © 2022 Student. All rights reserved.
//

import Foundation

class Podcast {
    let podcastName: String
    let podcastEpisodes: Int
    let podcastLogo: String
    
    init(_ podcastName: String, _ podcastEpisodes: Int, _ podcastLogo: String) {
        self.podcastName = podcastName
        self.podcastEpisodes = podcastEpisodes
        self.podcastLogo = podcastLogo
    }
}

class PodcastDAO {
    static func getList() -> [Podcast] {
        return [
            Podcast("70 over 70", 35, "70over70"),
            Podcast("The Trojan Horse Affair", 55, "horse"),
            Podcast("Dark House", 15, "darkhouse"),
            Podcast("Fat Mascara", 446, "fatmascara"),
            Podcast("The Lazy Genius", 297, "lazygenius"),
            Podcast("The Moth", 218, "moth"),
            Podcast("Dope Labs", 79, "dopelabs"),
            Podcast("Beautiful Anonymous", 354, "anonymous"),
            Podcast("Becoming Wise", 38, "wise"),
            Podcast("The Daily", 1626, "thedaily")
        ]
    }
}
