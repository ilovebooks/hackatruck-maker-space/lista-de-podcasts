//
//  PodcastTableViewCell.swift
//  PodcastTableVIew
//
//  Created by Student on 14/09/22.
//  Copyright © 2022 Student. All rights reserved.
//

import UIKit

class PodcastTableViewCell: UITableViewCell {
    @IBOutlet weak var podcastLogoImageVIew: UIImageView!
    @IBOutlet weak var podcastNameLabel: UILabel!
    @IBOutlet weak var podcastEpisodesLabel: UILabel!
}
